# IPSC Sandbox
Hra na seminář z informatiky

## Overview
Realistický IPSC tech-demo střelby na terče a běhání po herním levelu.

## Herní design
Simulace zbraní, střelby na terč. Hra by měla obsahovat pár herních levelu, sbírku různých zbraní.

## Potřebné výtvarné prvky (Assety)
Terče, zbraně, animace, objekty do scény, level.

## Level design/obsah hry
Různorodé prostředí venku.

## Uživatelské rozhraní
Ve hře žádné. V menu výběr zbraně a levelu.

## Mechaniky
<details><summary>Rozbalit</summary>

### Zbraně
- Animace částí zbraní při výstřelu
### Hráč
- Volný movement do všech směrů
- Pohled z první osoby
- Změna FOV při míření se zbraní
- Změna zbraní
### Level
- Různorodý level se statickými i dynamickými objekty
### Gameplay
- Terče a poppery počítající score
- Střelecký časovač
- IPSC Score koeficient

</details>

## Projektové řízení

### Členové týmu
Šimon Baloun, Sítek Výkora, Michal Drahokoupil

### Role

    Produkční - Šimon Baloun
    Umělecký Ředitel - Michal Drahokoupil
    Asset developer - Sítek Výkora
    Lead programmer - Sítek Výkora
    Developer - Šimon Baloun
    Developer - Michal Drahokoupil

### Termíny
Neurčeno
